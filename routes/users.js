var express = require("express");
var router = express.Router();

const USERS = [
  {
    id: 1,
    name: "Sandeep",
    age: "25",
    role: "Admin"
  }
];

/* GET users listing. */
router.get("/", (req, res, next) => {
  try {
    res.status(200).json(USERS);
  } catch (err) {
    res.status(500).json(err);
  }
});

/* GET user by Id. */
router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  try {
    const user = USERS.find(u => u.id == id);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({ message: "No Valid User for Provided Id" });
    }
  } catch (err) {
    res.status(500).json(err);
  }
});

/* POST user */

router.post("/", (req, res, next) => {
  const { name, age, role } = req.body;
  const id = USERS.length + 1;
  try {
    const user = USERS.find(u => u.id == id);
    if (user) {
      res
        .status(404)
        .json({ message: `User with name ${name} already Exist ` });
    } else {
      const createUser = USERS.push({ id, name, age, role });

      res.status(200).json(createUser);
    }
  } catch (err) {
    res.status(500).json(err);
  }
});

router.put("/:id", (req, res, next) => {
  const { name, age, role } = req.body;
  const id = req.params.id;
  try {
    const user = USERS.find(u => u.id == id);
    if (user) {
      const updatedUsers = USERS.map(user =>
        user.id == id ? { ...user, name, age, role } : user
      );
      res.status(200).json(updatedUsers);
    } else {
      res.status(404).json({ message: "No Valid User for Provided Id" });
    }
  } catch (err) {
    res.status(500).json(err);
  }
});

router.delete("/:id", (req, res, next) => {
  const { name, age, role } = req.body;
  const id = req.params.id;
  try {
    const user = USERS.find(u => u.id == id);
    if (user) {
      const updatedUsers = USERS.filter(u => u.id != id);
      res.status(200).json(updatedUsers);
    } else {
      res.status(404).json({ message: "No Valid User for Provided Id" });
    }
  } catch (err) {
    res.status(500).json(err);
  }
});
module.exports = router;
